# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_invoice_sale(self):
        invoice = super()._get_invoice_sale()
        if invoice:
            invoice.transaction_date = self._get_transaction_date()
            invoice.on_change_transaction_date()
        return invoice

    def _get_transaction_date(self):
        if self.moves:
            dates = [m.effective_date or m.planned_date
                for m in self.moves if (m.effective_date or m.planned_date)]
            if dates:
                return max(dates)
        return self.sale_date


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @property
    def transaction_date(self):
        if self.sale:
            return self.sale._get_transaction_date()


class SaleGrouping(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def _get_invoice_sale(self):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        Lang = pool.get('ir.lang')

        invoice = super()._get_invoice_sale()
        if self.check_invoice_transaction_date(invoice):
            date_fields = ['invoice_date', 'accounting_date']
            for date_field in date_fields:
                date = getattr(invoice, date_field, None)
                if date and invoice.transaction_date > date:
                    lang = Lang.get()
                    raise UserError(gettext('account_invoice_transaction_date'
                        '.msg_sale_incorrect_transaction_date',
                        sale=self.rec_name,
                        invoice=invoice.rec_name,
                        field=Invoice.fields_get(fields_names=[date_field]
                            )[date_field]['string'],
                        date_field=lang.strftime(date),
                        transaction_date=lang.strftime(
                            invoice.transaction_date)
                    ))
        return invoice

    def check_invoice_transaction_date(self, invoice):
        return invoice and invoice.transaction_date


class SaleInvoiceStandAlone(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def check_invoice_transaction_date(self, invoice):
        if (self.invoice_grouping_method
                and self.invoice_grouping_method == 'standalone'):
            return False
        return super().check_invoice_transaction_date(invoice)

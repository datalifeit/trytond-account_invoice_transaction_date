# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import sale
from . import purchase
from . import aeat_mapping
from . import account


def register():
    Pool.register(
        invoice.Invoice,
        invoice.InvoiceLine,
        account.GeneralLedgerLine,
        module='account_invoice_transaction_date', type_='model')
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        module='account_invoice_transaction_date', type_='model',
        depends=['sale'])
    Pool.register(
        purchase.Purchase,
        purchase.PurchaseLine,
        module='account_invoice_transaction_date', type_='model',
        depends=['purchase'])
    Pool.register(
        aeat_mapping.IssuedTrytonInvoiceMapper,
        aeat_mapping.RecievedTrytonInvoiceMapper,
        module='account_invoice_transaction_date', type_='model',
        depends=['aeat_sii'])
    Pool.register(
        invoice.Invoice2,
        module='account_invoice_transaction_date', type_='model',
        depends=['account_invoice_consecutive'])
    Pool.register(
        sale.SaleGrouping,
        module='account_invoice_transaction_date', type_='model',
        depends=['sale_invoice_grouping'])
    Pool.register(
        sale.SaleInvoiceStandAlone,
        module='account_invoice_transaction_date', type_='model',
        depends=['sale_invoice_line_standalone'])
    Pool.register(
        invoice.InvoiceSII,
        module='account_invoice_transaction_date', type_='model',
        depends=['account_es_sii'])

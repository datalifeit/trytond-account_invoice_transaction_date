==========================================
Transaction Date Invoice Grouping Scenario
==========================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()
    >>> tdate = today - relativedelta(days=10)

Install sale_invoice_grouping::

    >>> config = activate_modules(['account_invoice_transaction_date',
    ...     'sale_invoice_grouping'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer Grouped',
    ...     sale_invoice_grouping_method='standard')
    >>> customer.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale some products::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.sale_date = tdate
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.state
    'processing'

    >>> invoice, = sale.invoices
    >>> invoice.transaction_date == tdate
    True
    >>> invoice.accounting_date == tdate
    True

Make another sale::

    >>> sale, = Sale.duplicate([sale])
    >>> sale.sale_date = today
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Sale "2" cannot be grouped on invoice "(1)" because it has Accounting Date ..., which is lower than Operation date .... - 

Change date of sale::

    >>> sale.click('draft')
    >>> sale.sale_date = tdate
    >>> sale.click('quote')
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> sale.state
    'processing'

Check the invoices::

    >>> Invoice = Model.get('account.invoice')
    >>> invoices = Invoice.find([('party', '=', customer.id)])
    >>> len(invoices)
    1


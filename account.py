# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields


class GeneralLedgerLine(metaclass=PoolMeta):
    __name__ = 'account.general_ledger.line'

    transaction_date = fields.Function(fields.Date('Transaction Date'),
        'get_transaction_date', searcher='search_transaction_date')

    def get_transaction_date(self, name=None):
        pool = Pool()
        Invoice = pool.get('account.invoice')
        if isinstance(self.origin, Invoice):
            return self.origin.transaction_date
        elif isinstance(self.move.origin, Invoice):
            return self.move.origin.transaction_date
        return None

    @classmethod
    def search_transaction_date(cls, name, clause):
        return ['OR',
            [
                ('origin.%s' % clause[0],) + tuple(clause[1:3])
                + ('account.invoice',) + tuple(clause[3:])
            ],
            [
                ('move.origin.%s' % clause[0],) + tuple(clause[1:3])
                + ('account.invoice',) + tuple(clause[3:])
            ],
        ]

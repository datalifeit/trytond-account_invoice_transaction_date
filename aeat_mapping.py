# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from operator import attrgetter
from trytond.pool import PoolMeta

_DATE_FMT = '%d-%m-%Y'


class IssuedTrytonInvoiceMapper(metaclass=PoolMeta):
    __name__ = 'aeat.sii.issued.invoice.mapper'

    transaction_date = attrgetter('transaction_date')

    def build_issued_invoice(self, invoice):
        res = super().build_issued_invoice(invoice)
        res.update({
            'FechaOperacion': self.transaction_date(invoice).strftime(
                _DATE_FMT) if self.transaction_date(invoice) else None,
        })
        return res


class RecievedTrytonInvoiceMapper(metaclass=PoolMeta):
    __name__ = 'aeat.sii.recieved.invoice.mapper'

    transaction_date = attrgetter('transaction_date')

    def build_received_invoice(self, invoice):
        res = super().build_received_invoice(invoice)
        res.update({
            'FechaOperacion': self.transaction_date(invoice).strftime(
                _DATE_FMT) if self.transaction_date(invoice) else None,
        })
        return res
